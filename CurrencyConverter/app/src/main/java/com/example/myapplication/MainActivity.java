package com.example.myapplication;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Nation> arrayListNation = new ArrayList<>();
    Spinner spinner1;
    Spinner spinner2;

    public void getNations() {
        // Fetch list nations
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "http://api.geonames.org/countryInfoJSON?formatted=true&lang=it&countarrayListNationry=&username=leminhcuong2988&style=full";
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("geonames");
                    arrayListNation = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        arrayListNation.add(new Nation(jsonArray.getJSONObject(i)));
                    }

                    Spinner spinner1 = findViewById(R.id.spinner_1);
                    Spinner spinner2 = findViewById(R.id.spinner_2);

                    ArrayList<String> items = new ArrayList<>();
                    // get list select items
                    for (int i = 0; i < arrayListNation.size(); i++) {
                        items.add(
                                arrayListNation.get(i).getCountryName() +
                                        " - " +
                                        arrayListNation.get(i).getCurrencyCode());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, items);
                    spinner1.setAdapter(adapter);
                    spinner2.setAdapter(adapter);
                } catch (JSONException e) {
                    Log.d("aaa", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("aaa", error.toString());
            }
        });

        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init layout
        String[] items = new String[]{"Loading..."};
        ArrayAdapter<String> adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, items);
        spinner1 = findViewById(R.id.spinner_1);
        spinner2 = findViewById(R.id.spinner_2);
        spinner1.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        Button btnConvert = findViewById(R.id.btn_convert);
        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner1 = findViewById(R.id.spinner_1);
                spinner2 = findViewById(R.id.spinner_2);
                String spinner1Value = spinner1.getSelectedItem().toString();
                String spinner2Value = spinner2.getSelectedItem().toString();
                String countryCode1 = spinner1Value.split("-")[1].trim();
                String countryCode2 = spinner2Value.split("-")[1].trim();

                if (countryCode1.equals(countryCode2)) {
                    Toast.makeText(MainActivity.this, "Two Country are same", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (countryCode1.isEmpty() || countryCode2.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Please select country to convert", Toast.LENGTH_SHORT).show();
                    return;
                }
                new ReadRSS().execute(countryCode1, countryCode2);
            }
        });
        getNations();

    }

    public class ReadRSS extends AsyncTask<String, Void, Double> {
        private XmlPullParserFactory xmlFactoryObject;
        private XmlPullParser myParser;

        {
            try {
                xmlFactoryObject = XmlPullParserFactory.newInstance();
                myParser = xmlFactoryObject.newPullParser();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected Double doInBackground(String... countries) {
            try {
                Button btnConvert = findViewById(R.id.btn_convert);
                btnConvert.setText("WAITING...");
                URL url = new URL("https://" +
                        countries[0] + ".fxexchangerate.com/" +
                        countries[1] + ".xml");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);

                // Starts the query
                conn.connect();
                InputStream stream = conn.getInputStream();

                myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                myParser.setInput(stream, null);
                int event = myParser.getEventType();
                String text = "";
                ArrayList<String> descriptions = new ArrayList<>();
                while (event != XmlPullParser.END_DOCUMENT) {
                    String name = myParser.getName();

                    switch (event) {
                        case XmlPullParser.START_TAG:
                            break;

                        case XmlPullParser.TEXT:
                            text = myParser.getText();
                            break;
                        case XmlPullParser.END_TAG:
                            if (name.equals("description")) {
                                descriptions.add(text);
                            }
                            break;
                    }
                    event = myParser.next();
                }
                double rate = Double.parseDouble(descriptions.get(1).split("=")[2].trim().split(" ")[0]);
                return rate;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Double rate) {
            super.onPostExecute(rate);
            Button btnConvert = findViewById(R.id.btn_convert);
            btnConvert.setText("CONVERT");
            if (rate == null) {
                Toast.makeText(MainActivity.this, "Not support this country yet.", Toast.LENGTH_SHORT).show();
                return;
            }
            Log.d("aaa", rate + "");
            TextView tvResult = findViewById(R.id.tv_result);
            EditText etNumber = findViewById(R.id.et_number);
            TextView tvRate = findViewById(R.id.tv_rate);
            tvRate.setText("Rate is " + rate.toString());

            String number = etNumber.getText().toString();
            if (number.isEmpty()) {
                Toast.makeText(MainActivity.this, "Please input a number", Toast.LENGTH_SHORT).show();
                return;
            }
            double value = 0;
            try {
                value = Double.parseDouble(number);
            } catch (Exception ex) {
                Toast.makeText(MainActivity.this, "Please input a number", Toast.LENGTH_SHORT).show();
                return;
            }

            double result = value / rate;
            result = Math.round(result * 100) / 100;
            tvResult.setText(result + "");
        }
    }

}

