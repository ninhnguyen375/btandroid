package com.example.myapplication;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Nation implements Serializable {
    private String countryName, currencyCode;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Nation(String countryName, String currencyCode) {
        this.countryName = countryName;
        this.currencyCode = currencyCode;
    }

    public Nation(JSONObject jsonObject) throws JSONException {
        this.countryName = jsonObject.getString("countryName");
        this.currencyCode = jsonObject.getString("currencyCode").toLowerCase();
    }
}

