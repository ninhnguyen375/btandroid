package com.example.notepad;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class JsonUtil {
    public static JSONObject loadJsonFromFile(Context context, String fileName) throws IOException, JSONException {
        File file = new File(context.getFilesDir(), fileName);
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        StringBuilder stringBuilder = new StringBuilder();
        String line = bufferedReader.readLine();
        while (line != null){
            stringBuilder.append(line).append("\n");
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
        String response = stringBuilder.toString();
        JSONObject jsonObject  = new JSONObject(response);
        return jsonObject;
    }
    public static void saveJsonFile(Context context, String fileName, JSONObject json) throws IOException {
        String str = json.toString();
        File file = new File(context.getFilesDir(), fileName);
        Log.d("aaa", context.getFilesDir().toString());
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        bufferedWriter.write(str);
        bufferedWriter.close();
    }
}
