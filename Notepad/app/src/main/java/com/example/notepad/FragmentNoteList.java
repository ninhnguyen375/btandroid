package com.example.notepad;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

public class FragmentNoteList extends Fragment {
    TextView textView;
    FragmentDate fragmentDate;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note_list, container, false);
        textView = view.findViewById(R.id.listTodo);
        fragmentDate = (FragmentDate) getFragmentManager().findFragmentById(R.id.fragmentDate);

        try {
            JSONObject json = JsonUtil.loadJsonFromFile(getContext(), "note.json");
            fragmentDate.lastUpdate.setText(json.getString("lastUpdate"));
            textView.setText(json.getString("note"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String lastUpdate = fragmentDate.convertDate(new Date());
                fragmentDate.lastUpdate.setText(lastUpdate);
                try {
                    JSONObject json = new JSONObject();
                    json.put("lastUpdate", lastUpdate);
                    json.put("note", s.toString());
                    JsonUtil.saveJsonFile(getContext(), "note.json", json);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return view;
    }
}
